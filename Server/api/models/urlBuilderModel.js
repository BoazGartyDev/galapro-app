// define dataBase model - saving URL as string and creation time.
'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var URLSchema = new Schema({
    url: {
        type: String,
        required: 'Please enter default url link'
    },
    Created_date: {
        type: Date,
        default: Date.now
    },
});

module.exports = mongoose.model('SavedURL', URLSchema);