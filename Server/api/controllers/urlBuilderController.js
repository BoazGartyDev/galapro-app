'use strict';

var mongoose = require('mongoose'),
    Task = mongoose.model('SavedURL');


// Handle URL redirect logic
exports.handle_url = function (req, res) {


    let creationDate = new Date();
    let redirect = req.query.redirect == "true";
    let inputURL = req.query.url;
    console.log("Get URL called");
    let response = {};


    // redirect is true  - find entries in database.
    if (redirect) {
        Task.find({}).sort({ Created_date: 'desc' }).exec(function (err, task) {
            // redirect true and no entries in database send error message
            response.time = new Date() - creationDate
            if (err) {
                response.success = "false"
                response.error = err;
                res.json(response);
            }
            else {
                // No Entries in MongoDB
                if (task && task.length == 0) {
                    response.success = "false"
                    response.error = "No Default URL in dataBase";
                    res.json(response);
                    return;
                }
                if (err) {
                    res.send(err);
                    return;
                }
                else {
                    // get all entries from database stat sending url's to client.                    
                    io.on('connection', function (client) {
                        console.log('Client connected...');

                        client.on('join', function (data) {
                            console.log(data);
                        });

                        client.on('messages', function (data) {
                            // Got message from client start sending URL's.
                            console.log("sending message from server");
                            // loop over url array and sent to client after 10 seconds
                            for (let i = 0; i < task.length; i++) {
                                (function (index) {
                                    setTimeout(function () {
                                        console.log("task time is " + task[i].time);
                                        client.broadcast.emit('newUrl', task[i]);
                                    }, index * 10000);
                                })(i);
                            }


                        });

                    });

                }

                // we have entries in data, send last entry (default url). 
                response.success = "true"
                response.url = task[0].url;
                res.json(response);
            }

        });
    }
    // if redirect is false - send url came from client.
    else if (inputURL) {
        response.url = inputURL;
        response.success = "true"
        response.time = new Date() - creationDate;
        res.json(response)

    }
    else {
        response.success = "false";
        response.error = "Please enter valid url";
        response.time = new Date() - creationDate;
        res.json(response)

    }

    //);

};

exports.get_all = function (req, res) {
    Task.find({}, function (err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};



exports.add_url = function (req, res) {
    var new_task = new Task(req.body);
    new_task.save(function (err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};

exports.read_url = function (req, res) {
    Task.findById(req.params.urlId, function (err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};
exports.update_url = function (req, res) {
    Task.findOneAndUpdate({ _id: req.params.urlId }, req.body, { new: true }, function (err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};
exports.delete_url = function (req, res) {
    Task.remove({
        _id: req.params.urlId
    }, function (err, task) {
        if (err)
            res.send(err);
        res.json({ message: 'URL successfully deleted' });
    });
};
