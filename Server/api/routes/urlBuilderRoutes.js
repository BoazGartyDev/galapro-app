'use strict';
module.exports = function (app) {
    var urlList = require('../controllers/UrlBuilderController');

    // get URL from dataBase
    app.route('/handle_url/')
        .get(urlList.handle_url)

    // add new URL to database.
    app.route('/urls')
        .get(urlList.get_all)  // get all URLS       
        .post(urlList.add_url); // add new url

    // not required but nice to have
    app.route('/urls/:urlId')
        .get(urlList.read_url) // read specific entry from database.
        .put(urlList.update_url) // update specific entry (by ID) from database
        .delete(urlList.delete_url); //  delete url for id
};