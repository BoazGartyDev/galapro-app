import { TestBed } from '@angular/core/testing';

import { GalaNetworkService } from './gala-network.service';

describe('GalaNetworkService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GalaNetworkService = TestBed.get(GalaNetworkService);
    expect(service).toBeTruthy();
  });
});
