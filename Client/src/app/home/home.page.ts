import { Component } from '@angular/core';
import { GalaNetworkService } from './gala-network.service'
import { environment } from 'src/environments/environment';
import * as io from 'socket.io-client';
import { isValid } from 'ionicons/dist/types/icon/utils';
const SAVED_URL = "SAVED_URL";
const REDIRECT_CHECKED = "REDIRECT_CHECKED";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})


export class HomePage {

  private isChecked: string;
  urlInput: string;
  currUrl: string;
  private socket;
  private serverURL: string;

  // NetworkService - Angular service handles communication.
  constructor(private networkService: GalaNetworkService) {

  }

  ngOnInit(): void {
    this.serverURL = "";
    this.urlInput = ''
    this.isChecked = 'false';
    // open page  with saved data if available.
    let latest_url = localStorage.getItem(SAVED_URL)
    if (latest_url != null && latest_url != 'undefined')
      this.urlInput = latest_url;
    let is_checked = localStorage.getItem(REDIRECT_CHECKED);
    if (is_checked != "null")
      this.isChecked = localStorage.getItem(REDIRECT_CHECKED);
  }

  onSubmit() {
    // no input url and Redirect is false    
    if ((!this.urlInput || this.urlInput == '') && this.isChecked == "false") {
      //Show error message - better to use ionic alert, using alert for the assignment.
      alert("Please Enter a valid URL or enable redirect");
      return;
    }

    // store url and check box in localStorage.
    localStorage.setItem(SAVED_URL, this.urlInput);
    localStorage.setItem(REDIRECT_CHECKED, this.isChecked);

    /*
     The redirect is implemented as window.open can also use innerHtml or iframe.
     
     if request pass - start socket communication.
    */
    this.networkService.sendSubmitRequest(this.urlInput, this.isChecked).subscribe(response => {

      // Got error message from the server.
      if (response.success == "false") {
        //Show error message - better to use ionic alert, using alert for the assigment. 
        alert(response.error);
        return;
      }
      // do Redirect - if url is valid.
      if (this.isURL(response.url)) {
        window.open(response.url, '_system');
      }
      // if redirect is off don't continue with socket communication.
      if (this.isChecked == 'false')
        return;
      // operation time  - returned by the server (not socket operation)
      console.log("Redirect time " + response.time);

      // init socket - in production this should be used in Angular service.
      this.socket = io(environment.BaseURL);

      // Socket time calculated in client since we want to calculate time from sending the request until response.
      let startTime = new Date();
      // ask for url         
      this.socket.emit('messages', function (data) { });

      // get new URL and update the label view when received. 
      var self = this;
      this.socket.on('newUrl', function (urlObj) {
        console.log("got new URl " + urlObj.url);
        let time = Date.now() - +(startTime);
        console.log("Total socket response time " + time);
        self.serverURL = urlObj.url;
      });

    })

  }
  //Check if string is  valid URL.
  private isURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?' +
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|' +
      '((\\d{1,3}\\.){3}\\d{1,3}))' +
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' +
      '(\\?[;&a-z\\d%_.~+=-]*)?' +
      '(\\#[-a-z\\d_]*)?$', 'i');
    return pattern.test(str);
  }


}


