import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';


/*
We also have options to add ,update, delete urls on the server.
for this assigment just get default URL.
*/
const BASE_URL = "http://localhost:3000/";

const HANDLE_URL = "handle_url";
@Injectable({
  providedIn: 'root'
})
export class GalaNetworkService {
  //currentUrl = this.socket.fromEvent<Document>('newURL');  
  public defaultHeaders = new HttpHeaders();
  constructor(private httpClient: HttpClient) {

  }
  public sendSubmitRequest(
    url: String,
    isRedirect: String): Observable<any> {

    return this.httpClient.get<any>(
      `${BASE_URL}${HANDLE_URL}?url=${encodeURIComponent(String(url))}&redirect=${encodeURIComponent(String(isRedirect))}`,
      {
        headers: this.defaultHeaders,
      }

    );

  }
}
